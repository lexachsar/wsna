import numpy as np


def find_q_ij(lambda_ij: np.ndarray, lambda_i: np.ndarray) -> np.ndarray:
    """ Function finds the proportion of customers(demands) that go to node j from node i

    Функция находит q_ij -- отношение интенсивности поступления требований
    с узла i на узел j к суммарной интенсивности поступления требований на узел j

    Args:
        lambda_ij: Matrix of demand arrival rate from node i(line) to node j(column)
        lambda_i: Vector of total demand arrival rate at each node

    Returns:
        The matrix of proportions of customers that go to node j from node i

    """
    return lambda_ij / lambda_i


def find_v_j(q_ij: np.ndarray) -> np.ndarray:
    """ ToDo Функция находит

    Args:
        q_ij: The matrix of proportions of customers that go to node j from node i

    Returns:
        ToDo Функция возвращает

    """
    return np.power(np.sum(q_ij, axis=0), -1)


def find_omega_j(psi_j: np.ndarray, v_j: np.ndarray) -> np.ndarray:
    """ ToDo Функция находит

    Args:
        psi_j: Vector of traffic loads of each queuing system
        v_j: ToDo Describe parameter

    Returns:
        ToDo Функция возвращяет
    """
    return np.power((1 + 4 * np.power((1 - psi_j), 2) * (v_j - 1)), -1)


def find_x_i(L: int, c2_si: np.ndarray) -> np.ndarray:
    """ ToDo Describe Function

    Args:
        L: Number of internal queuing systems (nodes)
        c2_si: SCV of processing time of packets at node i

    Returns:
        ToDo Describe return value

    """
    x_i = []
    for i in range(1, L + 1):
        x_i.append(max(c2_si[i], 0.2))

    return np.transpose(x_i)


# TODO: Check this function
def find_b_ij(L: int, omega_j: np.ndarray, THETA: np.ndarray, q_ij: np.ndarray, psi: np.ndarray) -> np.ndarray:
    """ ToDo Describe

    Промежуточная величина, необходимая для рассчета c2_aj

    Args:
        L: Number of internal queuing systems (nodes)
        omega_j: ToDo Describe parameter
        THETA: Routing matrix
        q_ij: The matrix of proportions of customers that go to node j from node i
        psi: Vector of traffic loads of each queuing system

    Returns:
        ToDo Describe return value
    """
    b_ij = np.zeros((L, L))

    for i in range(0, L):
        for j in range(0, L):
            b_ij[i][j] = omega_j[j] * THETA[i][j] * q_ij[i][j] * (1 - np.power(psi[i], 2))

    return b_ij


# TODO: add exponenta
def find_g_j(L: int, psi_j: np.ndarray, c2_aj: np.ndarray, c2_sj: np.ndarray) -> np.ndarray:
    """ Function finds value, needed for further mean w_j calculation

    When c2_aj < 1, due to g_j value, mean w_j becomes Kraemer and Langenbach-Belz approximation.

    When c2_aj > 1, the original Kraemer and Langenbach-Belz approximation refinement does not seem
    to help, so it is not used, so g_j = 1.

    Args:
        L: Number of internal queuing systems (nodes)
        psi_j: Vector of traffic loads of each queuing system
        c2_aj: Vector of SCV of inter-arrival times of routed packets from other nodes to each node
        c2_sj: Vector of SCV of processing time of packets at each node

    Returns:
        Vector of g_j, 1<j<=L values that used in further w_j calculation
    """
    g_j = np.zeros(L)
    for i in range(0, L):
        # if(c2_aj[i] >= 1):
        g_j[i] = 1
    return g_j


def find_a_j(L: int, omega_j: np.ndarray, q_ij: np.ndarray, c2_0j: np.ndarray, THETA: np.ndarray, psi: np.ndarray,
             x_i: np.ndarray) -> np.ndarray:
    """ Function finds values, needed for further c2_aj calculation

    Промежуточная величина, необходимая для рассчета c2_aj

    Args:
        L: Number of internal queuing systems (nodes)
        omega_j: ToDo Describe parameter
        q_ij: The matrix of proportions of customers that go to node j from node i
        c2_0j: Vector of SCV of inter-arrival times of routed demands from source to each node
        THETA: Routing matrix
        psi: Vector of traffic loads of each queuing system
        x_i: ToDo Describe parameter

    Returns:
        Vector of values, needed for further c2_aj calculation

    """
    # Инициализация массива, в который будут записаны результаты работы функции
    a_j = np.zeros(L)

    for j in range(0, L):
        # Рассчитываем сумму из правой части формулы (15)
        SUM = 0
        for i in range(0, L):
            SUM += q_ij[i][j] * (1 - THETA[i][j] + THETA[i][j] * np.power(psi[i], 2) * x_i[i])
        # Рассчитываем a_j
        a_j[j] = 1 + omega_j[j] * ((q_ij[0][j] * c2_0j[j] - 1) + SUM)

    return a_j


def find_c2_aj(L: int, a_j: np.ndarray, b_ij: np.ndarray) -> np.ndarray:
    """ Function finds SCV for the arrival processes for each queuing system

    Функция рассчитывает ККВ длительности между поступления требований
    из других систем в систему i, решая СЛАУ (17)

    Args:
        L: Number of internal queuing systems (nodes)
        a_j: Vector of values calculated by find_a_j function
        b_ij: ToDo Describe parameter

    Returns:
        Vector of SCV for the arrival processes for each queuing system

    """
    A = np.array((L, L))
    B = np.array(L)

    # a_j остается в правой части
    B = a_j

    # SUM b_ij * c2_ai переносим в левую часть
    A = -b_ij.transpose()

    # Упрощаем, путем сложения c2_aj-ых
    I = np.eye(L, dtype=float)
    A = A + I

    # Решаем полученную СЛАУ
    return np.linalg.solve(A, B)


def find_c2_ij(L: int, q_ij: np.ndarray, psi: np.ndarray, c2_ai: np.ndarray, c2_si: np.ndarray) -> np.ndarray:
    """ Function finds SCV of inter-arrival times of routed packets from node i to node j

    Функция рассчитывает ККВ длительности между поступлениями требований из СМО i в СМО j

    Args:
        L: Number of internal queuing systems (nodes)
        q_ij: The matrix of proportions of customers that go to node j from node i
        psi: Vector of traffic loads of each queuing system
        c2_ai: Vector of SCV of inter-arrival times of routed packets from other nodes to each node
        c2_si: Vector of SCV of processing time of demands at each node

    Returns:
        Matrix of SCV of inter-arrival times of routed packets from node i(line) to node j(column)

    """
    # Инициализация массива, в который будут записаны результаты работы функции
    c2_ij = np.zeros((L, L))

    for i in range(0, L):
        for j in range(0, L):
            c2_ij[i][j] = q_ij[i][j] * (
                    1 + (1 - np.power(psi[i], 2)) * (c2_ai[i] - 1) + np.power(psi[i], 2) * (c2_si[i] - 1)) + 1 - \
                          q_ij[i][j]

    return c2_ij


def find_mean_w_j(L: int, lambda_sum: np.ndarray, c2_aj: np.ndarray, c2_sj: np.ndarray, g_j: np.ndarray,
                  psi: np.ndarray, mu: np.ndarray) -> np.ndarray:
    """ Function finds mean waiting time in each node queue

    Функция находит м.о. длительности пребывания требований в очереди системы j

    Args:
        L: Number of internal queuing systems (nodes)
        lambda_sum: Vector of total demand arrival rate at each node
        c2_aj: Vector of SCV of inter-arrival times of routed packets from other nodes to each node
        c2_sj: Vector of SCV of processing time of demands at each node
        g_j: Vector of g_j, 1<j<=L values that used in w_j calculation
        psi: Vector of traffic loads of each queuing system
        mu: Vector of average processing rates at each node

    Returns:
        Vector of mean waiting times in each node queue

    """
    mean_w_j = (mu * psi * (c2_aj + c2_sj) * g_j) / (2 * (1 - psi))

    return mean_w_j


def qna(L: int, lambda_ij: np.ndarray, lambda_i: np.ndarray, c2_0j: np.ndarray, psi: np.ndarray, c2_sj: np.ndarray,
        THETA: np.ndarray, mu: np.ndarray) -> np.ndarray:
    """ Main pipeline of queuing network analyzer

    The QNA is an approximation algorithm developed at Bell Laboratories to calculate the
    average queuing delay at each node of open queuing networks without intermittency and
    with large number of infinite buffer nodes.

    Args:
        L: Number of internal queuing systems (nodes)
        lambda_ij: Matrix of demand arrival rate from node i(line) to node j(column)
        lambda_i: Vector of total demand arrival rate at each node
        c2_0j: Vector of SCV of inter-arrival times of routed demands from source to each node
        psi: Vector of traffic loads of each queuing system
        c2_sj: Vector of SCV of processing time of packets at each node
        THETA: Routing matrix
        mu: Vector of average processing rates at each node

    Returns:
        mean_w_j -- Vector of mean waiting times in each node queue
    """

    q_ij = find_q_ij(lambda_ij, lambda_i)
    # print("q_ij", q_ij)

    v_j = find_v_j(q_ij)
    # print("v_j", v_j)

    omega_j = find_omega_j(psi, v_j)
    # print("omega_j", omega_j)

    x_i = find_x_i(L, np.ones(L + 1))
    # print("x_i", x_i)

    b_ij = find_b_ij(L, omega_j, THETA, q_ij, psi)
    # print("b_ij", b_ij)

    a_j = find_a_j(L, omega_j, q_ij, np.ones(L + 1), THETA, psi, x_i)
    # print("a_j", a_j)

    c2_aj = find_c2_aj(L, a_j, b_ij)
    # print("c2_aj", c2_aj)

    g_j = find_g_j(L, psi, c2_aj, c2_sj)
    # print("g_j", g_j)

    c2_ij = find_c2_ij(L, q_ij, psi, c2_aj, c2_sj)
    # print("c2_ij", c2_ij)

    mean_w_j = find_mean_w_j(L, lambda_i, c2_aj, c2_sj, g_j, psi, 1 / mu)
    # print("mean_w_j", mean_w_j)
    return mean_w_j
