# Traffic-Rate Equations

import numpy as np


def find_omega(L: int, THETA: np.ndarray) -> np.ndarray:
    """ Function calculates omega matrix

    Функция находит распределение вероятностей поступления требований в системы на системы уравнений (1).

    Args:
        L: Number of queuing systems
        THETA: Routing matrix

    Returns:
        Omega matrix

    """

    # Создаем единичную матрицу, необходимую для решения системы
    I = np.eye(L + 1, dtype=float)

    A = THETA.transpose() - I

    # Заполняем последнюю строчку матрицы A единицами
    A[L, :] = np.ones(L + 1, dtype=float)

    # Создаем матрицу-столбец B системы, состоящую из нулей с единицей в нижней строке
    B = np.zeros((L + 1, 1))
    B[L] = 1

    # Решаем систему, получаем распределение вероятностей поступления требований в системы
    return np.linalg.solve(A, B)


def find_lambda_sum(L: int, lambda0: float, omega: np.ndarray) -> np.ndarray:
    """ Function finds total demand arrival rate at each node

    Функция находит матрицу суммарных интенсивностей потоков в каждый из узлов

    Args:
        L: Number of queuing systems
        lambda0: Average demand from source income rate
        omega: ToDo (lexachsar@gmail.com): describe this parameter

    Returns:
        Total demand arrival rate at each node

    """
    # Создаем матрицу интенсивностей потоков
    lambda_sum = np.zeros(L + 1)
    lambda_sum[0] = lambda0

    # Рассчет интенсивностей потоков
    for i in range(1, L + 1):
        lambda_sum[i] = (omega[i] / omega[0]) * lambda_sum[0]
    return lambda_sum


def find_lambda_ij(L: int, THETA: np.ndarray, lambda_sum: np.ndarray) -> np.ndarray:
    """ Function finds matrix of demand arrival rates from node i(line) to node j(column)

    Args:
        THETA: Routing matrix
        lambda_sum: Vector of total demand arrival rate at each node
        L: Number of queuing systems (nodes)

    Returns:
        Matrix of demand arrival rate from each node to each node
    """

    # Создаем матрицу интенсивностей потоков
    lambda_ij = np.zeros((L + 1, L + 1))

    # Рассчет интенсивностей потоков
    for i in range(0, L + 1):
        for j in range(0, L + 1):
            lambda_ij[i][j] = THETA[i][j] * lambda_sum[i]

    return lambda_ij


def find_psi(lambda_i: np.ndarray, mu: np.ndarray) -> np.ndarray:
    """ Function finds vector of traffic loads of each queuing system

    Функция находит коэффициент использования обслуживающих приборов системы

    Args:
        lambda_i: Vector of total demand arrival rate at each node
        mu: Vector of average processing rates at each node

    Returns:
        Vector of traffic loads of each queuing system

    """
    print(lambda_i)
    print(mu)
    return lambda_i / mu


# ToDo(lexachsar@gmail.com) Delete this function or uncomment it
# Функция вычисляет пропускную способность сети.
# def find_tau(THETA, lambda_sum):
#    return np.sum(lambda_sum * THETA[:, 0]) - lambda_sum[0] * THETA[0][0]

def find_tau(lambda_ij: np.ndarray) -> float:
    """ Function finds throughput of the network

    Функция вычисляет пропускную способность сети.

    Args:
        lambda_ij: Matrix of demand arrival rate from each node to each node

    Returns:
        Throughput of the network value

    """
    return np.sum(lambda_ij[:, 0]) - lambda_ij[0, 0]


# ToDo(lexachsar@gmail.com) Document this function. Find out what it does
def find_sigma(lambda_ij: np.ndarray) -> float:
    """ Function finds average generation rate of packets in the network

    Args:
        lambda_ij: Matrix of demand arrival rate from each node to each node

    Returns:
        Average generation rate of packets in the network value

    """

    return np.sum(lambda_ij[0, :]) - lambda_ij[0, 0]


def find_lost_probability(sigma: float, TAU: float) -> float:
    """ Function finds probability that demand is lost due to intermittency in the communication

    Функция вычисляет вероятность того, что требование было потеряно в связи с помехами

    Args:
        sigma: Average generation rate of packets in the network
        TAU: Throughput of the network

    Returns:
        Probability that demand is lost due to intermittency in the communication

    """
    return (sigma - TAU) / sigma


# ToDo Find out about first two parameters
def find_mean_n(mu: np.ndarray, lambda_i: np.ndarray, psi: np.ndarray) -> np.ndarray:
    """ Function finds mean number of demands in each system

    Рассчет м.о. числа требований в системе

    Args:
        mu: Vector of average processing rates at each node
        lambda_i: Vector of total demand arrival rate at each node
        psi: Vector of traffic loads of each queuing system

    Returns:
        Vector of mean number of demands in each system

    """

    return psi / (1 - psi)


def find_mean_u(mu: np.ndarray, lambda_i: np.ndarray) -> np.ndarray:
    """ Function finds mean time of demand stay in the system ToDo Change description

    Рассчет м.о. длительности пребывания требований в системе

    Args:
        mu: Vector of average processing rates at each node
        lambda_i: Vector of total demand arrival rate at each node

    Returns:
        Vector of mean times of demand stay in each system

    """

    return 1 / (mu - lambda_i)